\documentclass{book}

\usepackage{amsmath}
\usepackage{graphicx}

\title{my first doc}
\date{2017-04-27}
\author{leva_kleva}

\begin{document}
	\pagenumbering{gobble}
	\pagenumbering{arabic}

	\flushleft{\textit {\normalsize
	Nonlinear Dynamics, Psychology, and Life
 	Sciences, Vol. 8, No. 3, July, 2004. 
		\textcopyright \ 2004 Society for Chaos Theory in
 	Psychology \& Life Sciences \\}}
 	
 	\section*{Dynamical Models of Love}
 	
	{\textbf {J. C. Sprott\footnote{ Correspondence address: Department of Physics, University of Wisconsin, 
Madison, Wisconsin 53706; e-mail: sprott@physics.wisc.edu. }}{\textit, University of Wisconsin, Madison } \\
	
\begin{flushleft}
	\textbf {Abstract.} 
\textit{ Following  a  suggestion  of  Strogatz,  this  paper  examines  a  
sequence  of  dynamical  models  invo
lving  coupled  ordinary  differential  
equations  describing  the  time-variation  of  the  love  or  hate  displayed  by  
individuals  in  a  romantic  relationship.    The  models  start  with  a  linear  
system  of  two  individuals  and  advance  to  love  triangles  and  finally  to  
include the effect of nonlinearities, which are shown to produce chaos.} \\
\end{flushleft}
\textit{ \textbf {Key Words.}} Love, romance, differential equations, chaos.

\section*{\center{INTRODUCTION}}}
 
\hspace{1cm} The power of mathematics has rarely been applied to the
dynamics of romance. In his book, Strogatz (1994) has a short section
on love affairs and several related mathematical exercises. Essentially
the same model was described earlier by Rapoport (1960), and it has also
been studied by Radzicki (1993). Related discrete dynamical models of
the verbal interaction of married couples have recently been proposed by
Gottman, Murray, Swanson, Tyson, \& Swanson (2002). Although
Strogatz’s model was originally intended more to motivate students than
as a serious description of love affairs, it makes several interesting and
plausible predictions and suggests extensions that produce an even wider
range of behavior. This paper is written in the same spirit and extends
the ideas to love triangles including nonlinearities, which are shown to
produce chaos. \par \hspace{1cm} An obvious difficulty in any model of love is defining what is
meant by love and quantifying it in some meaningful way (Sternberg \&
Barnes 1988). There are many types of love, including intimacy,
passion, and commitment (Sternberg, 1986), and each type consists of a complex mixture of feelings. In addition to love for another person,
there is love of oneself, love of life, love of humanity, and so forth.
Furthermore, the opposite of love may not be hate, since the two feelings
can coexist, and one can love some things about one’s partner and hate
others at the same time. It is obviously unrealistic to suppose that one’s
love is only influenced by one’s own feelings and the feelings of the
other person, independent of other influences and that the parameters that
characterize the interaction are unchanging, thereby excluding the
possibility of learning and adaptation (Scharfe \& Bartholomew, 1994).
/However, the goal here is to illustrate the complexity that can arise in
even a minimal dynamical model when the equations are nonlinear
and/or they involve three or more variables. While there is no limit to
the ways in which the models can be made more realistic by adding
additional phenomena and parameters, these embellishments almost
certainly only increase the likelihood of chaos, which is the main new
observation reported here. 

\section*{\center{SIMPLE LINEAR MODEL}}
\hspace{1cm} Strogatz (1994) considers a love affair between Romeo and
Juliet, where R(t) is Romeo’s love (or hate if negative) for Juliet at time t
and J(t) is Juliet’s love for Romeo. The simplest model is linear with 
	\begin{equation}
		\begin{split}
			\frac{dR}{dt}\ = aR + bJ \\
			\frac{dJ}{dt}\ = cR + dJ
		\end{split}
	\end{equation}
where a and b specify Romeo’s "romantic style," and c and d specify
Juliet’s style. The parameter a describes the extent to which Romeo is
encouraged by his own feelings, and b is the extent to which he is
encouraged by Juliet’s feelings. Gottman et al. (2002) use the term
“behavioral inertia” for the former and “influence function” for the latter,
although the inertia is greatest when a = 0. The resulting dynamics are
two-dimensional, governed by the initial conditions and the four
parameters, which may be positive or negative. 
\par \hspace{1cm} A similar linear model has been proposed by Rinaldi (1998a) in
which a constant term is added to each of the derivatives in Eq. 1 to
account for the appeal (or repulsion if negative) that each partner presents to the other in the absence of other feelings. Such a model is
more realistic since it allows feelings to grow from a state of indifference
and provides an equilibrium not characterized by complete apathy.
However, it does so at the expense of introducing two additional
parameters. While the existence of a non-apathetic equilibrium may be
very important to the individuals involved, it does not alter the dynamics
other than to move the origin of the RJ state space. 

\section*{\center{ROMANTIC STYLES}}
\hspace{1cm} Romeo can exhibit one of four romantic styles depending on the
signs of a and b, with names adapted from those suggested by Strogatz
(1994) and his students: 
\begin{enumerate}
	\item  \textit{Eager beaver}: a $>$ 0, b $>$ 0 (Romeo is encouraged by his own
feelings as well as Juliet’s.) 
	\item \textit{Narcissistic nerd}: a $>$ 0, b $<$ 0 (Romeo wants more of what
he feels but retreats from Juliet’s feelings.)
	\item \textit{Cautious (or secure) lover}: a $<$ 0, b $>$ 0 (Romeo retreats from
his own feelings but is encouraged by Juliets.)
	\item \textit{Hermit}: a $<$ 0, b $<$ 0 (Romeo retreats from his own feelings
as well as Juliets.)
\end{enumerate}

\par \hspace{1cm}Gragnani, Rinaldi, and Feichtinger (1997) use the terms "secure"
and "synergic" to refer to individuals with negative a and positive b,
respectively, and such people probably represent the majority of the
population. A secure individual (a $<$ 0) suppresses his feelings of love or
hate in a time -1/a when the other ceases to have feelings toward him,
such as at death. A non-synergic individual (b $<$ 0) or "nerd" is one who
hates to be loved and loves to be hated. Since Juliet can also exhibit four
styles, there are 16 possible pairings, each with its own dynamics,
although half of those correspond to an interchange of R and J. 

\section*{\center{LOVE DYNAMICS}}
\par \hspace{1cm}Equations 1 have a single equilibrium at R = J = 0, corresponding
to mutual apathy, or a loving plateau in Rinaldi’s (1988) model,
with behavior determined by the eigenvalues 
	\begin{equation}
		\lambda = \frac{a+d}{2}\ \pm \frac{1}{2}\ \sqrt{(a+d)^2-4(ad-bc)}
	\end{equation}
The solutions are real if $(a + d)^2 \geq 4(ad - bc)$ or a complex conjugate pair
otherwise. The corresponding dynamics in the RJ plane are summarized
in Fig.1. The complex conjugate solution describes a focus that is stable
(attracting) for a + d negative and unstable (repelling) if positive, in
which case all solutions are unbounded (they go to infinity). If a + d is
exactly zero, the solution cycles endlessly around a center at the origin.
The real solutions are of two types, a node if the eigenvalues are of the
same sign and a saddle otherwise. The node may either be stable (an
attractor) if both eigenvalues are negative or unstable (a repellor) if both
are positive. The saddle has a stable direction along which trajectories
approach the origin (the inset or stable manifold) and an unstable
direction along which they are repelled (the outset or unstable manifold).
Strogatz (1994) asks his students to consider a number of special pairings
of individuals as described in the following sections. 

\begin{figure*}[h!]
	\center{\includegraphics[width=\linewidth]{image1.png} \\ \textbf{Fig.1} Dynamics in the vicinity of an equilibrium point in two dimensions from Eq. 1.}
\end{figure*}

\section*{\center{OUT OF TOUCH WITH ONE’S OWN FEELINGS}}
Consider the special case with both Romeo and Juliet out of
touch with their own feelings (a = d = 0) and only responding to the
other. The eigenvalues are $\lambda = \pm srqt{bc}$ , and the dynamics then depend on
b and c, for which there are three combinations with the outcomes
indicated: 
\begin{enumerate}
	\item{\textit{Two lovers}: b $>$ 0, c $>$ 0 (Saddle, mutual love or mutual hate).}
	\item{\textit{Two nerds}: b $<$ 0, c $<$ 0 (Saddle, one loving and the otherhating).}
	\item{\textit{Nerd plus lover}: bc $<$ 0 (Center, endless cycle of love andhate).}
\end{enumerate}
\par \hspace{1cm} The outcome for Cases 1 and 2 depend on the initial conditions
(first impressions count) as does the size of the oscillation in Case 3.

\section*{\center{FIRE AND ICE }}
\par \hspace{1cm} Now consider the case where the two lovers are exact opposites
(c = -b and d = -a). The eigenvalues are $\lambda = \pm\sqrt{a^2 - b^2}$ , and the
dynamics then depend on a and b, for which there are two combinations:
\begin{enumerate}
	\item{\textit{Eager beaver plus hermit}: ab $>$ 0.}
	\item{\textit{Narcissistic nerd plus cautious lover}: ab $<$ 0} 
\end{enumerate}
The outcome depends on whether the individuals respond more to
themselves ($\mid a\mid > \mid b\mid$) or to the other ($\mid a\mid < \mid b\mid$). The former case leads to a saddle in which the eager beaver and hermit are at odds and the
narcissistic nerd and cautious lover are in love or at war, and the latter
leads to a center. Thus they can end up in any quadrant (all four
combinations of love and hate) or in a never-ending cycle, but never
apathetic.

\section*{\center{PEAS IN A POD}}
\par \hspace{1cm} Two romantic clones (c = b and d = a) have eigenvalues $\lambda = a \pm b$ and dynamics that depend on a and b. Cautious lovers with $\mid a\mid < \mid b\mid$ and
eager beavers end up in either a love fest or war depending on the initial
conditions. Hermits with $\mid a\mid < \mid b\mid$ and narcissistic nerds end up with one
loving and the other hating. Cautious lovers and hermits with $\mid a\mid > \mid b\mid$
end up in a state of mutual apathy. Oscillations are not possible. 

\section*{\center{ROMEO THE ROBOT}}
\par \hspace{1cm} Suppose Romeo’s feelings toward Juliet are unaffected by her
feelings for him (b = 0) as well as his feelings toward her (a = 0), so that
R is a constant and the eigenvalues are $\lambda = d$ and $\lambda = 0$. Then there is an
equilibrium in which Juliet’s feelings are given by J = -cR/d, which may
be positive or negative depending on the sign of R and her romantic
style. If Romeo loves Juliet (R $>$ 0), she will love him back only if she is
a cautious lover or a narcissistic nerd (cd $<$ 0). However, the equilibrium
is stable only if she is cautious (d $<$ 0). If she is narcissistic (d $>$ 0),
either her love will grow without bound or she will come to hate him
depending on the initial conditions, but her feelings will never die and
oscillations are not possible. 

\section*{\center{LOVE TRIANGLES }}
\par \hspace{1cm} Richer dynamics can occur if a third party is added, in part
because alliances can arise in which two members can ally against the
third. Suppose Romeo has a mistress, Guinevere, although the third
person could be a child or other relative. The state space is then sixdimensional
rather than two-dimensional because each person has
feelings for two others and there are twelve parameters if each can adopt
different styles toward the others, even when the natural appeal
considered by Rinaldi (1998a) is ignored.
\par \hspace{1cm} In the simplest case, Juliet and Guinevere would not know about
one another and Romeo would adopt the same romantic style toward
them both (he is an equal opportunity lover). The resulting fourdimensional
system becomes two decoupled two-dimensional systems
unless Romeo’s feelings for Juliet are somehow affected by Guinevere’s
feelings for him, and similarly for Guinevere. For simplicity, suppose
Guinevere’s feelings for Romeo affect his feelings for Juliet in a way that
is exactly pposite to the way Juliet’s feelings for him affect his feelings
for her, an si f Ju t. The corresponding model is then:
\begin{equation}
	\begin{split}
		&\frac{dR_{J}}{dt}\ = aR_{J} + b(J-G) \\
		&\frac{dJ}{dt}\ = cR_{J} + dJ \\
		&\frac{dR_{G}}{dt}\ = aR_{G} + b(G - J) \\
		&\frac{dG}{dt}\ = eR_{G} + fG 
	\end{split}
\end{equation}
\par \hspace{1cm}Note that in this simple model, his total love $R_{J}$ + $R_{G}$
increases without bound (if a $>$ 0) or dies (if a $<$ 0), independent of the
other parameters, although he may still have very different feelings
toward his lovers in the process ($R_{J} \neq R_{G}$), depending on their
characteristics. This curious result arises because he reacts to them
identically so that their effects on him exactly cancel. In some ways he
treats them as a single individual with characteristics given by their
average. This situation is similar to the case with two persons whose
love also dies or grows without bound except in the special case of a + d
= 0 and ad $>$ bc. The symmetry can be easily broken, but at the expense
of adding additional parameters to the model. 

\par \hspace{1cm}Solutions resembling Fig.1 are common, but other behaviors are
also possible, such as in Fig.2. The trajectory in $R_{J}J$ space appears to
intersect itself, but that is because this plane is a projection of a fourdimensional
dynamic. A mathematically astute Juliet would infer the
additional variables. All solutions either attract to the origin or go to
infinity. This particular case corresponds to an eager beaver Romeo (a = b = 1) whose lovers are both narcissistic nerds (c = e = –1 and d = f = 1).
The trajectory and outcome depend on the initial conditions, but for this
case, Romeo eventually loves Juliet and hates Guinevere, and they both
hate him.
\begin{figure*}[h!]
	\center{\includegraphics[width=\linewidth]{image2.png} \\ \textbf{Fig.2}  One solution of the linear love triangle in Eq. 3.}
\end{figure*}
\par \hspace{1cm} It is interesting to ask how Romeo fares when averaged over all
romantic styles and initial conditions. Taking the parameters a through f
and the initial conditions from a random Gaussian distribution with mean
zero and variance one shows that Romeo ends up hating one and loving
the other 82\% of the time and loving or hating both 8\% of the time. In
10\% of the cases everyone is apathetic, and no one can be apathetic
unless everyone is. In only 2\% of the cases is everyone in love, and
similarly for hate. Thus the prognosis for this arrangement is rather dim,
although someone is likely to enjoy it by experiencing mutual love in
43\% of the cases (at least until she discovers her competition!). 

\section*{\center{NONLINEAR EFFECTS}}
\par \hspace{1cm}The foregoing discussion involved only linear equations for
which the allowable dynamics are limited. There are countless ways to
introduce nonlinearities. Imagine that Romeo responds positively to
Juliet’s love, but if she loves him too much, he feels smothered and
reacts adversely. Conversely, if Juliet is sufficiently hostile, Romeo
might decide to be nice to her, in what Gottman et al. (2002) call the
"repair nonlinearity." Thus we could replace the bJ in Eq. 1 with the logistic function bJ(1 $- \mid J \mid$), which amounts to measuring J in units such
that J = 1 corresponds to the value at which her love become
counterproductive. Qualitatively similar results follow from the function
$bJ(1 - J^2)$, which is the case considered by Rinaldi (1998b) in his model
of the love felt by the $14^{th}$ century Italian poet Francis Petrarch (1304-
1374) toward his beautiful married platonic mistress Laura (Jones 1995).
Assuming the same for Juliet gives:
\begin{equation}
	\begin{split}
		\frac{dR}{dt}\ = aR + bJ(1 - \mid J\mid) \\
		\frac{dJ}{dt}\ = cR(1 - \mid R\mid) + dJ
	\end{split}
\end{equation}
There are now four equilibria, including the one at the origin. Figure 3
shows a stable focus in which an eager beaver Juliet (c = d = 1) leads a
hermit Romeo (a = b = -2) to a mutually loving state with R = J = 2. A
similar model for cautious (secure) lovers with a sigmoid nonlinearity
also gives stable equilibria (Rinaldi \& Gragnani 1998). Equations 4
apparently do not admit limit cycles, and there is no chaos since the
system is only two-dimensional.
\begin{figure*}[h!]
	\center{\includegraphics[width=\linewidth]{image3.png} \\ \textbf{Fig.3} One solution of the nonlinear Romeo-Juliet dynamic in Eq. 4.}
\end{figure*}
 \par \hspace{1cm}The same nonlinearity can be applied to the love triangle in Eq. 3
giving 
\begin{equation}
	\begin{split}
		&\frac{dR_{J}}{dt}\ = aR_{J} + b(J - G)(1- \mid J - G \mid) \\
		&\frac{dJ}{dt}\ = cR_{J}(1 - \mid R_{J} \mid) + dJ \\
		&\frac{dR_{G}}{dt}\ = aR_{G} +  b(G - J)(1 - \mid G - J \mid) \\
		&\frac{dG}{dt}\ = eR_{G}(1 - \mid R_{G} \mid) + fG
	\end{split}
\end{equation}
This system can exhibit chaos with strange attractors, an example of
which is in Fig.4. This case has a cautious lover Romeo (a = -3 and b =
4) and Guinevere (e = 2 and f = -1) and a narcissistic nerd Juliet (c = -7
and d = 2). The largest Lyapunov exponents (base-e) are 0.380, 0, and –
14.380, and the rate of contraction in state space (the sum of the
Lyapunov exponents) is 2a + c + f = -14. The Kaplan-Yorke dimension
is 2.026 (Sprott 2003). Figure 5 illustrates the effect of the positive
Lyapunov exponent on the time evolution of Romeo’s love for Juliet for
the same case as Fig.4 but with two initial conditions that are identical
except that Romeo’s love for Juliet differs by 1\%. The regions of
parameter space that admit chaos are relatively small, sandwiched
between cases that produce limit cycles and unbounded solutions. 

\begin{figure*}[h!]
	\center{\includegraphics[width=\linewidth]{image4.png} \\ \textbf{Fig.4} Strange attractor from the nonlinear love triangle in Eq.5.}
\end{figure*}

\begin{figure*}[h!]
	\center{\includegraphics[width=\linewidth]{image5.png} \\ \textbf{Fig.5} Chaotic evolution of Romeo’s love for Juliet from Eq. 5 showing
the effect of changing the initial conditions by 1\%.}
\end{figure*}

\section*{\center{CONCLUSIONS}}
\par \hspace{1cm} Simple linear models of love can produce surprisingly complex
dynamics, much of which rings true to common experience. Even
simple nonlinearities can produce chaos when there are three or more
variables. An interesting extension of the model would consider a group
of interacting individuals (a large family or love commune). The models
are gross simplifications since they assume that love is a simple scalar
variable and that individuals respond in a consistent and mechanical way
to their own love and to the love of others toward them without external
influences. 



\end{document}
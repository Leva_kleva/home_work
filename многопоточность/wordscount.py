from big_sorting import *

def open_file(nomber, keys, dict) : #функция создает маленький файл с номером nomber и записывает в него слова из списка keys и кол-во встреч dict[keys]
	f = open('small_file'+str(nomber)+'.txt', 'w') 
	keys.sort() #сортируем ключи
	for key in keys :
		f.write(key + ' ' + str(dict[key])+'\n')
	f.write('None None' + '\n') #добавляем для корректной работы программы
	f.close()
	return [], {}
	
f = open('input.txt', 'r') #файл со словами для чтения
dict = {} #словарик, в котором ключ= слово, значение= количество встреч данного слова
keys = [] #ключи словаря
index = 0 #номер текущего файла
count = 0 #счетчик считанных строк
#иду по строкам файла

def kek(count, index, keys, dict) : #если считали 100000 строк то вносим их в файл, иначе ничего не делаем
	if count < 100000 :
		return index, count, keys, dict
	else :
		keys, dict = open_file(index, keys, dict)
		return index+1, 0, keys, dict
		
for line in f : #разделяю большой файл на мелкие
	line = line[:-1] #убираю символ '\n' с конца 
	#заполняю словарь
	if dict.get(line) == None :
		dict[line] = 1
		keys.append(line)
		index, count, keys, dict = kek(count, index, keys, dict)
	else :
		dict[line] += 1
		index, count, keys, dict = kek(count, index, keys, dict)
	count += 1
	
if count != 0 : #добавляю остатки в новый файл
	keys, dict = open_file(index, keys, dict)
	index += 1
	count = 0
f.close()

def read(file) : #функция- считывает из файла строку и возвращает список или None в случае конца файла
	a, b =  file.readline().split()
	if a == b == 'None' :
		return None 
	else :
		b = int(b)
		return [a, b]

def check(mas) : #проверка на "дошли ли мы до конца во всех файлах?"
	flag = 0
	for i in range(len(mas)) :	
		if mas[i] == None :
			flag += 1
	return flag

g = open('alpha.txt', 'w') #выходной файл
file = [open('small_file'+str(i)+'.txt') for i in range(index)] #открываем недавно созданные файлы
temp = [] #храним минимум
for i in range(len(file)) :
	temp.append(read(file[i]))

while True :
	if check(temp) == len(file) : #проверка выхода
		break
	t, count = min(temp)[0], 0 #храним минимум, кол-во встреч слова
	for i in range(len(file)) : #проходимся по элементам файлов, если слова в них одни и те же, то суммируем их кол-во встреч
		if t == temp[i][0] :
			count += temp[i][1] #добавили кол-во встреч
			temp[i] = read(file[i]) #считываю след. строку в маленьком файле
	g.write(str(count) + ' ' + t + '\n') # записываю кол-во встреч
g.close()

#сортировка
sorting('alpha.txt') 
reverse('output.txt')

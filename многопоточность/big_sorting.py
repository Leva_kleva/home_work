def int_str(line) : #функция, которая считанную строку разделяет по прробелам и переделывает в список, при этом по возможности делает строку числом int, иначе оставляет строкой
	line = line.split()
	for i in range(len(line)) :
		try : #попытка присвоить строку к int'у
			line[i] = int(line[i])
		except ValueError :
			line[i] = str(line[i])
	return line #возвращает полученный список

def STR(mas) : #функция. По заданному списку делает строку, разделяя элемнты пробелом и добавляя в конце перевод строки
	s = ''
	for i in mas :
		s += str(i) + ' '
	s += '\n'
	return s

def merge(temp, temp_file) :
	import threading
	
	temp[0].sort()
	temp[1].sort()
	
	procces = [threading.Thread(target=sorting, args=(temp[i],)) for i in range(2)]
	for i in procces :
		i.start()
	for i in procces :
		i.join()

	##слияние двух отсортированных массивов в файл
	i, j = 0, 0
	print(temp)
	while True : 
		if temp[0][i] == temp[1][j] :
			temp_file.write(STR(temp[0][i]))
			temp_file.write(STR(temp[1][j]))
			i += 1
			j += 1
		elif temp[0][i] > temp[1][j] :
			temp_file.write(STR(temp[1][j]))
			j += 1
		else :
			temp_file.write(STR(temp[0][i]))
			i += 1	
		if i == len(temp[0])  :
			while j < len(temp[1]) :
				temp_file.write(STR(temp[1][j]))
				j += 1
			break
				
		if j == len(temp[1]) :
			while i < len(temp[1]) :
				temp_file.write(STR(temp[0][i]))
				i += 1
			break
				
	return temp, temp_file
	
def sorting(name_file) : #функция сортировки(от меньшего элемента к большему). Передаем название файла в формате 'name_file.txt'
	#разделение файла на маленькие
	file = open(str(name_file), 'r') #открываем файл, который нужно отсортировать
	temp_file = open('small_array'+str(0)+'.txt', 'w') #создаем маленький файл. Файл, который влазит в память
	index = 0 #переменная= номер заполняемого файла
	temp = [[], []] #массив, хранящий последние считанные строки
	for line in file : #считывание из файла строк
		line = int_str(line) 
		
		if len(temp[0]) < 10**1/2 :
			temp[0].append(line)
		else :
			temp[1].append(line)
			 
		if len(temp[1]) > 10**1/2-1 : #если считали 10**5 строк(столько в память входит) то записываем их в маленьких файл
			temp, temp_file = merge(temp, temp_file)
			#подчищаем свои следы
			temp[0].clear()
			temp[1].clear()
			index += 1
			temp_file.close()
			temp_file = open('small_array'+str(index)+'.txt','w')

			
	if len(temp[0]) != 0 : #если остались строки, которые мы считали но ни куда не внесли, то записываем их в последний файл
		if len(temp[1]) != 0 :
			temp, temp_file = merge(temp, temp_file)
		else :
			temp[0].sort()
			for el in temp[0] :
				temp_file.write(STR(el))
		temp[0].clear()
		temp[1].clear()
		index += 1
		temp_file.close()
        
	file.close() #закрываем входной файл
	
	#сама сортировка
	mas = [open('small_array'+str(i)+'.txt', 'r') for i in range(index)] #открываем все наши маленькие файлы
	g_file = open('output.txt', 'w') #открываем файл, в который запишем осортированный большой файл
	IND = [None for i in range(index)] #массив, в котором будут самые минимальные элемнты из каждого маленького файла в данный момент

	for i in range(index) : #заполняем его
		IND[i] = int_str(mas[i].readline())
		print(IND[i])

	while True : #цикл, в котором сливаем нащи файлы
		mini = [None, 0] #переменная для хранения минимального элемента
		for i in range(len(IND)) : #проходимся по минимальным элементам файла, которые еще не трогали и ищем минимум и запоминаем его номер
			if IND[i] != None : 
				if (mini != [None, 0] and mini[0] > IND[i]) or mini == [None, 0] :
					mini = [IND[i], i]
		if mini != [None, 0] : #если нашли, то записываем его и заменяем следующей строкой в файле
			g_file.write(STR(mini[0]))
			IND[mini[1]] = int_str(mas[mini[1]].readline())
			if IND[mini[1]] == [] : #если дошли до конца
				IND[mini[1]] = None 
		else : #иначе значит, что в IND остались все элементы None  то есть в каждом файле мы дошли до конца
			break		
	g_file.close()

def reverse(name_file) : #переворот файла (то есть первый элемент становится последним, пердпоследний вторым и тд). на фходе имя считываемого файла
	f = open(str(name_file), 'r')
	temp = []
	for line in f :
		temp.append(line)
	f.close()
	g = open('output.txt', 'w')
	for i in range(len(temp)-1,-1,-1) :
		g.write(temp[i])
	g.close()
	
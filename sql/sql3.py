
import sqlite3

#Подключение к базе
con = sqlite3.connect("workers.db")

#Создание курсора
c = con.cursor()


c.execute('''
DROP TABLE IF EXISTS Department
''')
c.execute('''
DROP TABLE IF EXISTS Employee
''')

con.commit()

#Создание таблиц
c.execute(
"""
CREATE TABLE Department (
   id int auto_increment primary key,
   name varchar(100)
)
""")

c.execute("""
CREATE TABLE Employee (
   id int auto_increment primary key,
   department_id int,
   chief_id int,
   name varchar(100),
   salary int,
   FOREIGN KEY (department_id) REFERENCES Department(id),
   FOREIGN KEY (chief_id) REFERENCES Employee(id)
)
""")


#Наполнение таблицы
values = [(1, 'робототехники'), (2, 'шаурмичный'), (3, 'котиков'), (4, 'счастья')]
c.executemany("""INSERT INTO Department (id, name) VALUES (?, ?)""", values)


values = [
   (1, 1, 1, "Петя", 200),
   (2, 2, 1, "Валя", 100),
   (3, 4, 1, "Аня", 400),
   (4, 3, 1, "Соня", 500),
   (5, 2, 2, "Рита", 500),
   (6, 2, 2, "Кеша", 200),
   (7, 3, 4, "Гена", 300),
   (8, 2, 4, "Варя", 900)
]
c.executemany("""INSERT INTO Employee (id, department_id, chief_id, name, salary) VALUES (?, ?, ?, ?, ?)""", values)
#Подтверждение отправки данных в базу
con.commit()


c.execute("""
select * from Department
""")

#print(c.fetchall())

c.execute("""
select * from Employee
""")

#zadaniya
# #1
sql_select = '''
SELECT name FROM (SELECT e1.id, e1.salary as w_s, e1.name,  e2.salary as c_s FROM Employee as e1
JOIN
Employee as e2
ON e1.chief_id = e2.id) as e3
WHERE e3.w_s > e3.c_s
'''

c.execute(sql_select)
print(c.fetchall())

# #2
sql_select = '''
SELECT name FROM (SELECT e1.name, e1.salary, e1.department_id FROM Employee as e1
JOIN
(SELECT MAX(salary) as max_salary, department_id FROM Employee GROUP BY department_id) as e2
ON e1.department_id = e2.department_id and e1.salary = e2.max_salary) as e3
'''

c.execute(sql_select)
print(c.fetchall())

# #3
sql_select = '''
SELECT * FROM
(SELECT e1.name, e1.id, e2.count_d FROM Department as e1
JOIN
(SELECT COUNT(*) as count_d, department_id FROM Employee GROUP BY department_id) as e2
ON e1.id = e2.department_id) as e3
WHERE e3.count_d <= 3
'''

c.execute(sql_select)
print(c.fetchall())

# #5
sql_select = '''
SELECT * FROM ((SELECT department_id, SUM(salary) as salary_s FROM Employee GROUP BY department_id) as e3
JOIN
(SELECT * FROM 
(SELECT MAX(salary_s) as m_S FROM
(SELECT department_id, SUM(salary) as salary_s FROM Employee GROUP BY department_id) as e1)) as e2
ON e2.m_S = e3.salary_s) as e4
'''

c.execute(sql_select)
print(c.fetchall())


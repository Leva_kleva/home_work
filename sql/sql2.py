import sqlite3

con = sqlite3.connect('sql2.db')
cur = con.cursor()

#удаление
sql_drop = '''
DROP TABLE IF EXISTS workers
'''
cur.execute(sql_drop)
con.commit()

cur.execute('''
CREATE TABLE workers
(
id INTEGER,
name VARCHAR(228),
age INTEGER,
salary INTEGER
);
''')
con.commit()


k = [
    (1, 'Дима', 23, 400),
    (2, 'Петя', 25, 500),
    (3, 'Вася', 23, 500),
    (4, 'Коля', 30, 1000),
    (5, 'Иван', 27, 500),
    (6, 'Кирилл', 26, 1000),
    (1000, 'LEVA_KLEVA', 228, 1000000000000)
    ]
# заполнение
for element in k :
    cur.execute("INSERT INTO workers VALUES (?,?,?,?)", element)

con.commit()


#задания
# #1
sql_select = '''
SELECT * FROM workers
WHERE id = '6'
'''

cur.execute(sql_select)
print(cur.fetchall())

# #2
sql_select = '''
SELECT * FROM workers
WHERE salary = '500'
'''

cur.execute(sql_select)
print(cur.fetchall())

# #3
sql_select = '''
SELECT * FROM workers
WHERE salary = '500' AND id > '3'
'''

cur.execute(sql_select)
print(cur.fetchall())


# #4
cur.execute("INSERT INTO workers VALUES (?,?,?,?)", (7, 'Джон', 20, 700))
con.commit()

# #5
cur.executemany("INSERT INTO workers VALUES (?,?,?,?)", ((8, 'Катя', 20, 500), (9, 'Юля', 25, 600), (10, 'Женя', 30, 900)))
con.commit()

# #6
cur.execute("DELETE FROM workers WHERE name = 'Джон'")
con.commit()

# #7
cur.execute("UPDATE workers SET salary = '1000' WHERE name = 'Дима'")
con.commit()


# #8
cur.execute("UPDATE workers SET salary = '1488', age = '228' WHERE name = 'Дима'")
con.commit()

# #9
sql_select = '''
SELECT * FROM workers
WHERE 25 < age <= 28
'''

cur.execute(sql_select)
print(cur.fetchall())


# #10
sql_select = '''
SELECT * FROM workers
WHERE name != 'Петя'
'''

cur.execute(sql_select)
print(cur.fetchall())

# #11
sql_select = '''
SELECT * FROM workers
WHERE age = '27' or salary = '1000'
'''

cur.execute(sql_select)
print(cur.fetchall())


# #12
sql_select = '''
SELECT * FROM workers
WHERE ('23' <= age < '27') or salary = '1000'
'''

cur.execute(sql_select)
print(cur.fetchall())


# #13
sql_select = '''
SELECT * FROM workers
WHERE ('23' <= age <= '27') or ('400' <= salary <= '1000')
'''

cur.execute(sql_select)
print(cur.fetchall())


# #14
sql_select = '''
SELECT * FROM workers
WHERE age = '27' or salary = '1000'
'''

cur.execute(sql_select)
print(cur.fetchall())


#check
print()
sql_select = '''
SELECT * FROM workers
'''
cur.execute(sql_select)
print(cur.fetchall())

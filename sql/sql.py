import sqlite3

    ###таблица роботов
con = sqlite3.connect('main.db')
cur = con.cursor()

sql_create1 = '''
CREATE TABLE robots
(
id INTEGER PRIMARY KEY,
name VARCHAR(255)
);
'''

k1 =[] #данные

sql_insert1 = '''
INSERT INTO robots (id, name)
VALUES (1, "a")
''' #добавить данные

cur.execute(sql_create1)
con.commit()

cur.execute(sql_insert1)
con.commit()



    ###поставщики
sql_create2 = '''
CREATE TABLE providers
(
id INTEGER PRIMARY KEY,
name VARCHAR(255),
address_physical VARCHAR(255),
address_actual VARCHAR(255)
);
'''

k2 = [] #данные

sql_insert2 = '''
INSERT INTO providers (id, name, address_physical, address_actual)
VALUES (1, "a", "a", "a")
''' #добавить данные

cur.execute(sql_create2)
con.commit()

cur.execute(sql_insert2)
con.commit()


    ###загатовки
sql_create3 = '''
CREATE TABLE blanks
(
id INTEGER,
name VARCHAR(255),
quantity INTEGER,
price INTEGER,
id_providers INTEGER,
time VARCHAR(255),
FOREIGN KEY(id_providers) REFERENCES providers(id)
);
'''

k3 =[] #данные

sql_insert3 = '''
INSERT INTO blanks (id, name, quantity, price, id_providers, time)
VALUES (1, "a", 1, 1, 1, "1")
''' #добавить данные

cur.execute(sql_create3)
con.commit()

cur.execute(sql_insert3)
con.commit()


    ###товар
sql_create4 = '''
CREATE TABLE product
(
id INTEGER PRIMARY KEY,
name VARCHAR(255),
price REAL
);
'''

k4 = [] #данные

sql_insert4 = '''
INSERT INTO product (id, name, price)
VALUES (1, "a", 1)
''' #добавить данные

cur.execute(sql_create4)
con.commit()

cur.execute(sql_insert4)
con.commit()


    ###рецепт
sql_create5 = '''
CREATE TABLE receipt
(
id INTEGER,
id_product INTEGER,
ingredient_1 VARCHAR(255),
FOREIGN KEY(id_product) REFERENCES product(id)
);
'''

k5 = [] #данные

sql_insert5 = '''
INSERT INTO receipt (id, id_product, ingredient_1)
VALUES (1, 1, "a")
''' #добавить данные

cur.execute(sql_create5)
con.commit()

cur.execute(sql_insert5)
con.commit()


    ###Selling
sql_create6 = '''
CREATE TABLE selling
(
id INTEGER,
id_product INTEGER,
id_robot INTEGER,
quantity INTEGER,
time VARCHAR(255),
FOREIGN KEY(id_robot) REFERENCES robots(id),
FOREIGN KEY(id_product) REFERENCES product(id)
);
'''

k6 =[] #данные

sql_insert6 = '''
INSERT INTO selling (id, id_product, id_robot, quantity, time)
VALUES (1, 1, 1, 1, "1")
''' #добавить данные

cur.execute(sql_create6)
con.commit()

cur.execute(sql_insert6)
con.commit()



cur.close()
con.close()
